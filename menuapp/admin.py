from django.contrib import admin
from menuapp.models import *


@admin.register(MenusModel)
class MenusAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    list_filter = ['name']


@admin.register(SubmenusModel)
class SubmenusAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    list_filter = ['name']

@admin.register(DishesModel)
class DishesAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']
    list_filter = ['name']
    search_fields = ['name']
