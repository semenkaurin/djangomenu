from uuid import UUID

from django import template
from menuapp.models import *


register = template.Library()


@register.simple_tag()
def get_submenus(menu_id: UUID):
    menu = MenusModel.objects.get(id=menu_id)
    submenus = SubmenusModel.objects.filter(menu_id=menu.id)

    return submenus
