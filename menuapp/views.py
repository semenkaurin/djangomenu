from uuid import UUID

from django.shortcuts import render

from menuapp.models import MenusModel, DishesModel


def get_menu_by_id(request, menu_id: UUID):
    '''Возвращает страницу меню по его id'''
    menu = MenusModel.objects.get(id=menu_id)
    dishes = DishesModel.objects.all()
    context = {
        'menu': menu,
        'menu_id': menu_id,
        'dishes': dishes,
    }
    return render(request, 'menuapp/menu.html', context=context)


def get_all_menus(request):
    '''Возвращает все меню доступные в бд'''
    menus = MenusModel.objects.all()
    context = {
        'menus': menus
    }
    return render(request, 'menuapp/all_menus.html', context=context)
