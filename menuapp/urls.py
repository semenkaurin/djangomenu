from django.urls import path

from menuapp import views


urlpatterns = [
    path('<str:menu_id>/', views.get_menu_by_id, name='menu'),
    path('', views.get_all_menus)
]
