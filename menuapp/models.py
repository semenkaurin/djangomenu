import uuid

from django.db import models
from django.shortcuts import reverse


# Create your models here.
class MenusModel(models.Model):
    '''Модель данных представляющая меню'''
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    name = models.CharField(max_length=255, unique=True)

    def get_absolute_url(self):
        return reverse('menu', kwargs={'menu_id': self.id})

    def __str__(self):
        return self.name


class SubmenusModel(models.Model):
    '''Модель данных представляющая подменю'''
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    name = models.CharField(max_length=255, unique=True)

    menu = models.ForeignKey(MenusModel, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class DishesModel(models.Model):
    '''Модель данных представляющая блюда подменю'''
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    name = models.CharField(max_length=255, unique=True)
    submenu = models.ForeignKey(SubmenusModel, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
