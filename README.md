# Django Menu Backend

## Стек технологий
- python3.10
- SQLite
- Django

## Установка

### 1. Перейти в папку проекта
*Это папка с файлом `main.py`
Все дальшейшие действия совершаются внутри неё*

### 2. Создать и активировать виртуальное окружение
```
python -m venv venv
source venv/bin/activate
```
*venv - путь к папке виртуального окружения
можно оставить просто venv, и тогда папка создастся в текущей*

### 3. Установить зависимости
```
pip install -r requirements.txt
```
### 4. Применить миграции
```
python manage.py migrate
```

### 5. Стартовать сервер Django из проекта
```
python manage.py runserver
```
